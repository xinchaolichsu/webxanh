
<div id="content" class="content-homepage">
    <div id="slide-wrapper">
        <ul id="slider">
            <li>
                <div class="border_on_img"></div>
                <div class="content_slider">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                </div>
                <img src="../dist/css/image/ex/s1.jpg" alt="Lorem ipsum dolor sit amet, consectetuer adipiscing elit."> </li>
            <li>
                <div class="border_on_img"></div>
                <div class="content_slider">
                    <p>sed diam nonummy</p>
                </div>
                <img src="../dist/css/image/ex/s2.jpg" alt="sed diam nonummy"></li>
            <li>
                <div class="border_on_img"></div>
                <div class="content_slider">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                </div>
                <img src="../dist/css/image/ex/s3.jpg" alt="Lorem ipsum dolor sit amet, consectetuer adipiscing elit."> </li>
        </ul>
    </div>
    <div class="banner">
        <div>
            <a href="specials.html"><img src="image/small-banner-green-225x161.png" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit"></a>
        </div>
    </div>
    <div class="banner">
        <div><img src="image/small-banner-blue-225x161.png" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit"> </div>
    </div>
    <div class="box lastest-product-home">
        <div>
            <h1 class="title_module"><span>Latest Products</span></h1>
            <div class="box-content">
                <?php 
                    global $product;
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 8
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();
                 ?>
                    <?php echo $__env->make('templates.loop-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php 
                        endwhile;
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                 ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="box sale-product-home">
        <div>
            <h1 class="title_module"><span>Sale Products</span></h1>
            <div class="box-content">
                <?php 
                $query_args = array(
                    'posts_per_page'    => 8,
                    'no_found_rows'     => 1,
                    'post_status'       => 'publish',
                    'post_type'         => 'product',
                    'meta_query'        => WC()->query->get_meta_query(),
                    'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() )
                );
                $loop = new WP_Query( $query_args );
                if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();
                 ?>
                    <?php echo $__env->make('templates.loop-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php 
                        endwhile;
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                 ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="box">
        <div>
            <h1 class="title_module"><span>Featured Products</span></h1>
            <div class="box-content">
                <div class="box-product">
                    <a class="image" href="product.html" title="View more"> <img src="../dist/css/image/ex/p3-209x179.jpg" alt=""> </a>
                    <h3 class="name"><a href="product.html" title="">Brown jacket for boys and girls</a></h3>
                    <p class="wrap_price"> <span class="price">$589.50</span> </p>
                    <p class="submit">
                        <input type="button" value="Add to Cart" class="button">
                    </p>
                </div>
                <div class="box-product">
                    <a class="image" href="product.html" title="View more"> <img src="../dist/css/image/ex/p4-209x179.jpg" alt=""> </a>
                    <h3 class="name"><a href="product.html" title="">Funny toy in green, red and blue</a></h3>
                    <p class="wrap_price"> <span class="price">$120.68</span> </p>
                    <p class="submit">
                        <input type="button" value="Add to Cart" class="button">
                    </p>
                </div>
                <div class="box-product">
                    <a class="image" href="product.html" title="View more"> <img src="../dist/css/image/ex/p5-209x179.jpg" alt=""> <span class="new">Sale</span> </a>
                    <h3 class="name"><a href="product.html" title="">Brown shoes for boys</a></h3>
                    <p class="wrap_price"> <span class="price-old">$119.50</span> <span class="price-new">$107.75</span> </p>
                    <p class="submit">
                        <input type="button" value="Add to Cart" class="button">
                    </p>
                </div>
                <div class="box-product last-item">
                    <a class="image" href="product.html" title="View more"> <img src="../dist/css/image/ex/p6-209x179.jpg" alt=""> </a>
                    <h3 class="name"><a href="product.html" title="">Locomotive in green, red and blue</a></h3>
                    <p class="wrap_price"> <span class="price">$236.99</span> </p>
                    <p class="submit">
                        <input type="button" value="Add to Cart" class="button">
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div id="carousel">
        <ul class=" jcarousel-skin-tfc">
            <li>
                <a href="brands.html"><img src="../dist/css/image/ex/b1.jpg" alt="" title=""></a>
            </li>
            <li>
                <a href="brands.html"><img src="../dist/css/image/ex/b2.jpg" alt="" title=""></a>
            </li>
            <li>
                <a href="brands.html"><img src="../dist/css/image/ex/b3.jpg" alt="" title=""></a>
            </li>
            <li>
                <a href="brands.html"><img src="../dist/css/image/ex/b4.jpg" alt="" title=""></a>
            </li>
            <li>
                <a href="brands.html"><img src="../dist/css/image/ex/b5.jpg" alt="" title=""></a>
            </li>
            <li>
                <a href="brands.html"><img src="../dist/css/image/ex/b6.jpg" alt="" title=""></a>
            </li>
        </ul>
    </div>
</div>

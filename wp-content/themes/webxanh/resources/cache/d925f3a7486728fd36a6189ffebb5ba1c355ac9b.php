<div class="footer_wrapper">
  <div id="footer_bottom">
    <div class="footer_bottom_item">
      <h3 class="bottom_item_1 down"><a>Information</a></h3>
      <ul class="menu_footer_item text_item">
        <li><a href="about.html" title="About Us">About Us</a></li>
        <li><a href="blog.html" title="Delivery Information">Blog</a></li>
        <li><a href="comparison.html" title="Privacy Policy">Compare List</a></li>
        <li><a href="#" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
      </ul>
    </div>
    <div class="footer_bottom_item">
      <h3 class="bottom_item_2 down"><a>Customer Service</a></h3>
      <ul class="menu_footer_item text_item">
        <li><a href="contact.html">Contact Us</a></li>
        <li><a href="#">Returns</a></li>
        <li><a href="#">Site Map</a></li>
      </ul>
    </div>
    <div class="footer_bottom_item">
      <h3 class="bottom_item_3 down"><a>Extras</a></h3>
      <ul class="menu_footer_item text_item">
        <li><a href="brands.html">Brands</a></li>
        <li><a href="gifts.html">Gift Vouchers</a></li>
        <li><a href="#">Affiliates</a></li>
        <li><a href="specials.html">Specials</a></li>
      </ul>
    </div>
    <div class="footer_bottom_item">
      <h3 class="bottom_item_4 down"><a>My Account</a></h3>
      <ul class="menu_footer_item text_item">
        <li><a href="myaccount.html">My Account</a></li>
        <li><a href="orderhistory.html">Order History</a></li>
        <li><a href="wishlist.html">Wish List</a></li>
        <li><a href="#">Newsletter</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
  <div id="mobile-footer">
    <div class="mobile-footer-menu">
      <h3>Information</h3>
      <div class="mobile-footer-nav" style="display: none;">
        <ul>
          <li><a href="about.html" title="About Us">About Us</a></li>
          <li><a href="blog.html" title="Delivery Information">Blog</a></li>
          <li><a href="comparison.html" title="Privacy Policy">Compare List</a></li>
          <li><a href="#" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
        </ul>
      </div>
      <h3>Customer Service</h3>
      <div class="mobile-footer-nav" style="display: none;">
        <ul>
          <li><a href="contact.html">Contact Us</a></li>
          <li><a href="#">Returns</a></li>
          <li><a href="#">Site Map</a></li>
        </ul>
      </div>
      <h3>Extras</h3>
      <div class="mobile-footer-nav" style="display: none;">
        <ul>
          <li><a href="brands.html">Brands</a></li>
          <li><a href="gifts.html">Gift Vouchers</a></li>
          <li><a href="#">Affiliates</a></li>
          <li><a href="specials.html">Specials</a></li>
        </ul>
      </div>
      <h3>My Account</h3>
      <div class="mobile-footer-nav" style="display: none;">
        <ul>
          <li><a href="myaccount.html">My Account</a></li>
          <li><a href="orderhistory.html">Order History</a></li>
          <li><a href="wishlist.html">Wish List</a></li>
          <li><a href="#">Newsletter</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="footer-text">
  <p>Things for Cuties © 2013 - Template by <a href="http://themeforest.net/user/ssievert?ref=ssievert">ssievert</a></p>
</div>

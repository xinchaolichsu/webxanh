<?php 
global $product;
 ?>
<div class="box-product">
    <a class="image" href="<?php echo e(get_permalink()); ?>" title="View more"> <?php echo get_the_post_thumbnail(); ?> 
    <?php if( $product->is_on_sale()): ?>
    	<span class="new">Sale</span> </a>
    <?php endif; ?>
    <h3 class="name"><a href="product.html" title=""><?php echo e(the_title()); ?></a></h3>
    <p class="wrap_price"> <span class="price-old"><?php echo $product->get_price_html(); ?></span> <span class="price-new"><?php echo $product->get_price_html(); ?></span> </p>
    <p class="submit">
        <a href="?add-to-cart=<?php echo e($product->id); ?>"><input type="button" value="<?php echo e($product->add_to_cart_text()); ?>" class="button"></a>
    </p>
</div>
@php
global $product;
@endphp
<div class="box-product">
    <a class="image" href="{{ get_permalink() }}" title="View more"> {!! get_the_post_thumbnail() !!} 
    @if ( $product->is_on_sale())
    	<span class="new">Sale</span> </a>
    @endif
    <h3 class="name"><a href="product.html" title="">{{ the_title() }}</a></h3>
    <p class="wrap_price"> <span class="price-old">{!! $product->get_price_html() !!}</span> <span class="price-new">{!! $product->get_price_html() !!}</span> </p>
    <p class="submit">
        <a href="?add-to-cart={{ $product->id }}"><input type="button" value="{{ $product->add_to_cart_text() }}" class="button"></a>
    </p>
</div>
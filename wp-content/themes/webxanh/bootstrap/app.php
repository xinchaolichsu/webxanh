<?php
require __DIR__ . '/../vendor/autoload.php';

/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'themeEnqueueScripts');
add_action('wp_enqueue_scripts', 'themeEnqueueStyle');
/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function themeEnqueueStyle()
{
    wp_enqueue_style('template-style', get_stylesheet_directory_uri() . '/dist/css/app.min.css', false);
}

function themeEnqueueScripts()
{
    wp_enqueue_script('template-scripts', get_stylesheet_directory_uri() . '/dist/js/scripts.js', ['jquery']);
    wp_enqueue_script('template-browserify', get_stylesheet_directory_uri() . '/dist/js/app.js', ['template-scripts']);
}

/**
 * helpers
 */
function asset($path)
{
    return substr($path, 0, 1) == '/' ? get_stylesheet_directory_uri() . '/assets' . $path : get_stylesheet_directory_uri() . '/assets/' . $path;
}
